/* version.c --- Version handling self tests.
 * Copyright (C) 2003-2022 Simon Josefsson
 *
 * This file is part of the GNU Generic Security Service Library.
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include <stdio.h>		/* printf */
#include <stdlib.h>		/* EXIT_SUCCESS */
#include <string.h>		/* strcmp */

/* Get GSS prototypes. */
#include <gss.h>

int
main (int argc, char *argv[])
{
  int exit_code = EXIT_SUCCESS;
  char *out = NULL;
  int j;
  unsigned gvn = GSS_VERSION_NUMBER;
  unsigned gvmmp = (GSS_VERSION_MAJOR << 16)
    + (GSS_VERSION_MINOR << 8) + GSS_VERSION_PATCH;

  printf ("GSS_VERSION: %s\n", GSS_VERSION);
  printf ("GSS_VERSION_MAJOR: %d\n", GSS_VERSION_MAJOR);
  printf ("GSS_VERSION_MINOR: %d\n", GSS_VERSION_MINOR);
  printf ("GSS_VERSION_PATCH: %d\n", GSS_VERSION_PATCH);
  printf ("GSS_VERSION_NUMBER: %x\n", gvn);
  printf ("(GSS_VERSION_MAJOR << 16) + (GSS_VERSION_MINOR << 8)"
	  " + GSS_VERSION_PATCH: %x\n", gvmmp);

  j = asprintf (&out, "%d.%d.%d", GSS_VERSION_MAJOR,
		GSS_VERSION_MINOR, GSS_VERSION_PATCH);
  if (j < 0)
    {
      printf ("asprintf failure: %d", j);
      exit_code = EXIT_FAILURE;
      out = NULL;
    }

  if (out)
    printf ("GSS_VERSION_MAJOR.GSS_VERSION_MINOR.GSS_VERSION_PATCH: %s\n",
	    out);

  printf ("gss_check_version (NULL): %s\n", gss_check_version (NULL));

  if (!gss_check_version (GSS_VERSION))
    {
      printf ("FAIL: gss_check_version (GSS_VERSION)\n");
      exit_code = EXIT_FAILURE;
    }

  if (!gss_check_version ("1.0.1"))
    {
      printf ("FAIL: gss_check_version (1.0.1)\n");
      exit_code = EXIT_FAILURE;
    }

  if (strcmp (GSS_VERSION, gss_check_version (NULL)) != 0)
    {
      printf ("FAIL: strcmp (GSS_VERSION, gss_check_version (NULL))\n");
      exit_code = EXIT_FAILURE;
    }

  if (GSS_VERSION_NUMBER != gvn)
    {
      printf ("FAIL: GSS_VERSION_NUMBER != gvn\n");
      exit_code = EXIT_FAILURE;
    }

  if (out)
    {
      if (!gss_check_version (out))
	{
	  printf ("FAIL: gss_check_version(%s)\n", out);
	  exit_code = EXIT_FAILURE;
	}

      /* GSS_VERSION may look like "1.0.4.10-b872" but the derived string
         should be "1.0.4" anyway.  */
      if (strncmp (GSS_VERSION, out, strlen (out)) != 0)
	{
	  printf ("FAIL: strncmp (GSS_VERSION, %s, strlen (%s))\n", out, out);
	  exit_code = EXIT_FAILURE;
	}

      free (out);
    }

  if (gss_check_version ("4711.42.23"))
    {
      printf ("FAIL: gss_check_version(4711.42.23)\n");
      exit_code = EXIT_FAILURE;
    }

  if (gss_check_version ("UNKNOWN"))
    {
      printf ("FAIL: gss_check_version (UNKNOWN)\n");
      exit_code = EXIT_FAILURE;
    }

  return exit_code;
}
