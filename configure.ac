# Copyright (C) 2003-2022 Simon Josefsson
#
# This file is part of the GNU Generic Security Service Library.
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

AC_PREREQ([2.64])
AC_INIT([GNU Generic Security Service],
        m4_esyscmd([build-aux/git-version-gen .tarball-version]),
        [bug-gss@gnu.org],
        [gss])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])

# Library code modified:                              REVISION++
# Interfaces changed/added/removed:   CURRENT++       REVISION=0
# Interfaces added:                             AGE++
# Interfaces removed:                           AGE=0
AC_SUBST(LT_CURRENT, 3)
AC_SUBST(LT_REVISION, 3)
AC_SUBST(LT_AGE, 0)

# Used when creating libgss-XX.def.
DLL_VERSION=`expr ${LT_CURRENT} - ${LT_AGE}`
AC_SUBST(DLL_VERSION)

AC_SUBST([MAJOR_VERSION], [`echo $PACKAGE_VERSION|cut -d. -f1`])
AC_SUBST([MINOR_VERSION], [`echo $PACKAGE_VERSION|cut -d. -f2`])
AC_SUBST([PATCH_VERSION], [`echo $PACKAGE_VERSION.0|cut -d. -f3|cut -d- -f1`])
AC_SUBST(NUMBER_VERSION, `printf "0x%02x%02x%02x" $MAJOR_VERSION $MINOR_VERSION $PATCH_VERSION 2>/dev/null`)

AC_MSG_NOTICE([$PACKAGE_NAME $PACKAGE_VERSION $PACKAGE_TARNAME $MAJOR_VERSION.$MINOR_VERSION.$PATCH_VERSION $NUMBER_VERSION $LT_CURRENT:$LT_REVISION:$LT_AGE $DLL_VERSION])

AM_INIT_AUTOMAKE([1.10 -Wall -Wno-override])
AM_SILENT_RULES([yes])
AC_CONFIG_HEADERS(config.h)

# Checks for programs.
AC_PROG_CC
gl_EARLY
libgl_EARLY
AM_PROG_AR
LT_INIT([win32-dll])
AM_MISSING_PROG(PERL, perl, $missing_dir)
AM_MISSING_PROG(HELP2MAN, help2man, $missing_dir)

# Internationalization.
AM_GNU_GETTEXT([external])
AM_GNU_GETTEXT_VERSION([0.19.8])

# https://developer.gnome.org/gtk-doc-manual/stable/settingup_autoconf.html.en
m4_ifdef([GTK_DOC_CHECK], [
GTK_DOC_CHECK([1.14],[--flavour no-tmpl])
],[
AM_CONDITIONAL([ENABLE_GTK_DOC], false)
])

# For gnulib stuff.
gl_INIT
libgl_INIT
AC_DEFINE([GNULIB_NO_VLA], 1, [Disable VLA usage in gettext.h.])

# Test for Shishi.
AC_ARG_ENABLE(kerberos5,
  AS_HELP_STRING([--disable-kerberos5],
    [disable Kerberos V5 mechanism unconditionally]),
  kerberos5=$enableval)
if test "$kerberos5" != "no" ; then
  AC_LIB_HAVE_LINKFLAGS(shishi,, [#include <shishi.h>],
                        [shishi_key_timestamp (0);])
  if test "$ac_cv_libshishi" = yes; then
    AC_DEFINE([USE_KERBEROS5], 1, [Define to 1 if you want Kerberos 5 mech.])
    INCLUDE_GSS_KRB5='# include <gss/krb5.h>'
    INCLUDE_GSS_KRB5_EXT='# include <gss/krb5-ext.h>'
    kerberos5=yes
  else
    kerberos5=no
  fi
fi
AC_MSG_CHECKING([if the Kerberos V5 mechanism should be supported])
AC_MSG_RESULT($kerberos5)
AM_CONDITIONAL(KRB5, test "$kerberos5" = "yes")
AC_SUBST(INCLUDE_GSS_KRB5)
AC_SUBST(INCLUDE_GSS_KRB5_EXT)

sj_PO_SUFFIX($DLL_VERSION)

AC_ARG_ENABLE([gcc-warnings],
  [AS_HELP_STRING([--enable-gcc-warnings],
		  [turn on lots of GCC warnings (for developers)])],
  [case $enableval in
     yes|no) ;;
     *)      AC_MSG_ERROR([bad value $enableval for gcc-warnings option]) ;;
   esac
   gl_gcc_warnings=$enableval],
  [gl_gcc_warnings=no]
)

if test "$gl_gcc_warnings" = yes; then
  gl_WARN_ADD([-Werror], [WERROR_CFLAGS])

  nw="$nw -Wsystem-headers"            # Ignore errors in system headers
  nw="$nw -Wc++-compat"                # We don't care much about C++ compilers
  nw="$nw -Wconversion"                # Too many warnings for now
  nw="$nw -Wsign-conversion"           # Too many warnings for now
  nw="$nw -Wcast-qual"                 # Too many warnings for now
  nw="$nw -Wtraditional"               # Warns on #elif which we use often
  nw="$nw -Wunreachable-code"          # False positive on strcmp
  nw="$nw -Wpadded"                    # Standard GSS-API headers are unpadded
  nw="$nw -Wtraditional-conversion"    # Too many warnings for now
  nw="$nw -Wsuggest-attribute=pure"    # Is it worth using attributes?
  nw="$nw -Wsuggest-attribute=const"   # Is it worth using attributes?

  gl_MANYWARN_ALL_GCC([ws])
  gl_MANYWARN_COMPLEMENT(ws, [$ws], [$nw])
  for w in $ws; do
    gl_WARN_ADD([$w])
  done

  gl_WARN_ADD([-Wno-unused-parameter])
  gl_WARN_ADD([-Wno-stack-protector])  # Some functions cannot be protected
  gl_WARN_ADD([-Wno-array-bounds])     # gcc-4.6 meta.c false positive
  gl_WARN_ADD([-fdiagnostics-show-option])
fi

AC_CONFIG_FILES([
  Makefile
  doc/Makefile
  doc/cyclo/Makefile
  doc/reference/Makefile
  lib/Makefile
  lib/gl/Makefile
  lib/gss.pc
  lib/headers/gss.h
  lib/krb5/Makefile
  po/Makefile.in
  src/Makefile
  src/gl/Makefile
  tests/Makefile
])
AC_OUTPUT

AC_MSG_NOTICE([summary of main build options:

  version:            ${VERSION}
  Libtool version     $LT_CURRENT:$LT_REVISION:$LT_AGE
  DLL version:        $DLL_VERSION
  Header version:     major $MAJOR_VERSION minor $MINOR_VERSION patch $PATCH_VERSION number $NUMBER_VERSION
  Host/Target system: ${host}
  Build system:       ${build}
  Install prefix:     ${prefix}
  Compiler:           cc: ${CC} cflags: ${CFLAGS} cppflags: ${CPPFLAGS}
  Valgrind:           $opt_valgrind_tests ${VALGRIND}
  Library types:      Shared=${enable_shared}, Static=${enable_static}
  Warning flags:      $gl_gcc_warnings werror $WERROR_CFLAGS warn $WARN_CFLAGS

  Version script:     $have_ld_version_script
  I18n domain suffix: ${PO_SUFFIX:-none}

  Kerberos V5:        $kerberos5
        LDADD:        $LTLIBSHISHI
])
