/* krb5/utils.c --- Kerberos 5 GSS-API helper functions.
 * Copyright (C) 2003-2022 Simon Josefsson
 *
 * This file is part of the GNU Generic Security Service Library.
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at
 *    your option) any later version.
 *
 * or
 *
 * * the GNU General Public License as published by the Free Software
 *   Foundation; either version 2 of the License, or (at your option)
 *   any later version.
 *
 * or both in parallel, as here.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License
 * and the GNU Lesser General Public License along with this file.  If
 * not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Get specification. */
#include "k5internal.h"

/* Return number of seconds left of ticket lifetime, or 0 if ticket
   has expired, or GSS_C_INDEFINITE if ticket is NULL. */
OM_uint32
gss_krb5_tktlifetime (Shishi_tkt * tkt)
{
  time_t now, end;

  if (!tkt)
    return GSS_C_INDEFINITE;

  if (!shishi_tkt_valid_now_p (tkt))
    return 0;

  now = time (NULL);
  end = shishi_tkt_endctime (tkt);

  return end - now;
}
