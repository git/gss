variables:
  GIT_DEPTH: 50
  GIT_SUBMODULE_STRATEGY: normal

stages:
 - build
 - test
 - deploy

.test:
  interruptible: true
  artifacts:
    expire_in: 2 weeks
    when: always
    paths:
    - "*.tar.gz"
    - ./*.log
    - ./config.h
    - ./**/*.log
    - clang-analyzer
    - doc/reference/html
    - doc/reference/*.pdf
    - doc/manual
    - coverage
    - cyclo

Debian:
  extends: .test
  image: debian:bullseye-slim
  stage: build
  before_script:
  - apt-get update -qq | tail -10
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq git autoconf automake libtool autopoint gettext cvs make texinfo texlive-font-utils help2man gengetopt indent libshishi-dev | tail -10
  script:
  - ./bootstrap --skip-po
  - ./configure --enable-gcc-warnings --disable-silent-rules
  - make syntax-check
  - make check
  - grep 'PASS krb5context' tests/krb5context.log
  - make dist
  - git diff --exit-code # nothing should change version controlled files

Ubuntu-22.04:
  extends: .test
  image: ubuntu:22.04
  stage: build
  before_script:
  - apt-get update -qq | tail -10
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq git autoconf automake libtool autopoint gettext cvs make texinfo texlive-font-utils help2man gtk-doc-tools libglib2.0-dev libshishi-dev gengetopt indent | tail -10
  script:
  - ./bootstrap --skip-po
  - ./configure --enable-gcc-warnings --disable-silent-rules
  - make syntax-check check
  - grep 'PASS krb5context' tests/krb5context.log
  - make dist
  - git diff --exit-code # nothing should change version controlled files
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q texlive texlive-plain-generic texlive-extra-utils gpg gpgv2
  - gpg --batch --passphrase '' --quick-gen-key bug-gss@gnu.org
  - git checkout -b fake-release
  - git config user.email "bug-gss@gnu.org"
  - git config user.name "GNU SASL CICD Builder"
  - git config user.signingkey bug-gss@gnu.org
  - sed -i '5i ** WARNING This release was prepared automatically with no testing.\n' NEWS
  - git commit -m "Warn about automatic release." NEWS
  - make release-commit RELEASE='17.42.23 stable'
  - make
  - make release RELEASE='17.42.23 stable'

AlmaLinux8:
  extends: .test
  image: almalinux:8
  stage: build
  before_script:
  - dnf -y update
  - dnf -y install epel-release
  - dnf --enablerepo=powertools -y install git make diffutils file hostname patch autoconf automake libtool gettext-devel texinfo help2man gengetopt
  script:
  - ./bootstrap --skip-po
  - ./configure --enable-gcc-warnings --disable-silent-rules
  - make check V=1 VERBOSE=t
  - make syntax-check
  - make install
  - git diff --exit-code # nothing should change version controlled files

Ubuntu-coverage-cyclo:
  extends: .test
  image: ubuntu:rolling
  stage: build
  before_script:
  - apt-get update -qq
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq git autoconf automake libtool autopoint gettext make texinfo texlive texlive-plain-generic texlive-extra-utils texlive-font-utils help2man gtk-doc-tools libglib2.0-dev valgrind gengetopt wget libshishi-dev
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq lcov pmccabe
  script:
  - ./bootstrap
  - ./configure --enable-gcc-warnings --disable-silent-rules CFLAGS="-g --coverage"
  - lcov --directory . --zerocounters
  - make check VERBOSE=t
  - grep 'PASS krb5context' tests/krb5context.log
  - make dist
  - make syntax-check
  - mkdir coverage
  - lcov --directory . --output-file coverage/gss.info --capture
  - lcov --remove coverage/gss.info '/usr/include/*' '*/gl/*' '*/gltests/*' '*/lib/gl/*' '*/lib/gltests/*' -o coverage/gss_filtered.info
  - genhtml --output-directory coverage coverage/gss_filtered.info --highlight --frames --legend --title "GNU SASL"
  - mkdir cyclo
  - make -C doc/cyclo/ cyclo-gss.html
  - cp -v doc/cyclo/cyclo-gss.html cyclo/index.html
  - git diff --exit-code # nothing should change version controlled files

Fedora-clanganalyzer:
  extends: .test
  image: fedora:latest
  stage: build
  before_script:
  - dnf update -y
  - dnf install -y git make diffutils file hostname patch autoconf automake libtool gettext-devel texinfo texinfo-tex texlive texlive-supertabular texlive-framed texlive-morefloats texlive-quotchap texlive-epstopdf help2man gtk-doc gengetopt gperf wget dblatex
  - dnf install -y clang clang-analyzer
  script:
  - ./bootstrap
  - scan-build ./configure --disable-silent-rules --enable-gtk-doc --enable-gtk-doc-pdf
  - scan-build -o clang-analyzer make V=1
  - make web-manual
  - make dist
  - git diff --exit-code # nothing should change version controlled files

.pages:
  stage: deploy
  needs: ["Ubuntu-coverage-cyclo", "Fedora-clanganalyzer"]
  script:
    - mkdir public
    - mv coverage/ cyclo/ public/
    - mv clang-analyzer/* public/clang-analyzer
    - mv doc/manual public/manual
    - mv doc/reference/html public/reference
    - mv doc/reference/gss.pdf public/reference/
  artifacts:
    paths:
    - public
    expire_in: 30 days

pages:
  extends: .pages
  only:
    - master

pages-test:
  extends: .pages
  except:
    - master

Ubuntu-14.04:
  extends: .test
  image: ubuntu:14.04
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc valgrind libshishi-dev
  script:
  - tar xfa gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t
  - grep 'PASS krb5context' tests/krb5context.log

Ubuntu-20.04:
  extends: .test
  image: ubuntu:20.04
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc valgrind libshishi-dev
  script:
  - tar xfa gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t
  - grep 'PASS krb5context' tests/krb5context.log

Ubuntu-distcheck:
  extends: .test
  image: ubuntu:devel
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc gtk-doc-tools texinfo texlive texlive-plain-generic valgrind libshishi-dev
  script:
  - tar xfa gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check distcheck V=1 VERBOSE=t
  - grep 'PASS krb5context' tests/krb5context.log

CentOS7:
  extends: .test
  image: centos:7
  stage: test
  needs: [Debian]
  before_script:
  - yum -y install make gcc diffutils
  script:
  - tar xfa gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t

Alpine:
  extends: .test
  image: alpine:latest
  stage: test
  needs: [Debian]
  before_script:
  - echo "ipv6" >> /etc/modules
  - apk update
  - apk add build-base gettext gettext-dev valgrind
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - sed -i 's/UNKNOWN/1.2.3/g' tests/version.c # XXX FIXME alpine bug?
  - make check V=1 VERBOSE=t

ArchLinux:
  extends: .test
  image: archlinux:latest
  stage: test
  needs: [Debian]
  before_script:
  - pacman -Syu --noconfirm make gcc diffutils file
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure
  - make check V=1 VERBOSE=t

Debian8:
  extends: .test
  image: debian:stretch-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc indent libshishi-dev
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t
  - make syntax-check
  - grep 'PASS krb5context' tests/krb5context.log

Debian9:
  extends: .test
  image: debian:buster-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc libshishi-dev
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t
  - grep 'PASS krb5context' tests/krb5context.log

Debian10:
  extends: .test
  image: debian:stretch-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc libshishi-dev
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - ./configure --disable-gcc-warnings
  - make check V=1 VERBOSE=t
  - grep 'PASS krb5context' tests/krb5context.log

Debian11-armcross:
  extends: .test
  image: debian:bullseye-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -qq
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc-arm-linux-gnueabi qemu-user qemu-user-binfmt file
  - update-binfmts --enable qemu-arm
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure --host=arm-linux-gnueabi --enable-gcc-warnings CFLAGS="-static --static"
  - make V=1
  - arm-linux-gnueabi-readelf -h lib/.libs/libgss.a
  - arm-linux-gnueabi-readelf -h src/gss
  - file src/gss
  - LANG=C.UTF-8 qemu-arm src/gss --major 196608
  - make check VERBOSE=t

Mingw32:
  extends: .test
  image: debian:latest
  stage: test
  needs: [Debian]
  before_script:
    - dpkg --add-architecture i386
    - apt-get update -q
    - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make mingw-w64 wine wine32 libwine libwine:i386 binfmt-support wine-binfmt
    - update-binfmts --enable wine
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure --host=i686-w64-mingw32 CC='i686-w64-mingw32-gcc -static-libgcc' --enable-gcc-warnings
  - make check V=1 VERBOSE=t WERROR_CFLAGS="-Werror -Wno-error=unused-value -Wno-error=suggest-attribute=format"

Mingw64:
  extends: .test
  image: debian:latest
  stage: test
  needs: [Debian]
  before_script:
    - apt-get update -q
    - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make mingw-w64 wine wine64 binfmt-support wine-binfmt
    - update-binfmts --enable wine
  script:
  - tar xfz gss-*.tar.gz
  - cd `ls -d gss-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure --host=x86_64-w64-mingw32 --enable-gcc-warnings
  - make check V=1 VERBOSE=t WERROR_CFLAGS="-Werror -Wno-error=unused-value -Wno-error=suggest-attribute=format"
