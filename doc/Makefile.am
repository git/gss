# Copyright (C) 2003-2022 Simon Josefsson
#
# This file is part of the GNU Generic Security Service Library.
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

SUBDIRS = cyclo
if ENABLE_GTK_DOC
SUBDIRS += reference
endif

EXTRA_DIST = gdoc asciidoc asciidoc.conf

info_TEXINFOS = gss.texi
gss_TEXINFOS = fdl-1.3.texi $(gdoc_TEXINFOS)

dist_man_MANS = gss.1 $(gdoc_MANS)
MAINTAINERCLEANFILES = $(dist_man_MANS)

gss.1: $(top_srcdir)/src/gss.c $(top_srcdir)/src/gss.ggo \
		$(top_srcdir)/.version
	$(HELP2MAN) \
		--name="Generic Security Service command line interface" \
		--output=$@ $(top_builddir)/src/gss$(EXEEXT)

# GDOC

gdoc_TEXINFOS =
gdoc_TEXINFOS += texi/gss_encapsulate_token.texi
gdoc_TEXINFOS += texi/gss_decapsulate_token.texi
gdoc_TEXINFOS += texi/gss_init_sec_context.texi
gdoc_TEXINFOS += texi/gss_accept_sec_context.texi
gdoc_TEXINFOS += texi/gss_delete_sec_context.texi
gdoc_TEXINFOS += texi/gss_process_context_token.texi
gdoc_TEXINFOS += texi/gss_context_time.texi
gdoc_TEXINFOS += texi/gss_inquire_context.texi
gdoc_TEXINFOS += texi/gss_wrap_size_limit.texi
gdoc_TEXINFOS += texi/gss_export_sec_context.texi
gdoc_TEXINFOS += texi/gss_import_sec_context.texi
gdoc_TEXINFOS += texi/gss_acquire_cred.texi
gdoc_TEXINFOS += texi/gss_add_cred.texi
gdoc_TEXINFOS += texi/gss_inquire_cred.texi
gdoc_TEXINFOS += texi/gss_inquire_cred_by_mech.texi
gdoc_TEXINFOS += texi/gss_release_cred.texi
gdoc_TEXINFOS += texi/gss_display_status.texi
gdoc_TEXINFOS += texi/gss_userok.texi
gdoc_TEXINFOS += texi/gss_create_empty_oid_set.texi
gdoc_TEXINFOS += texi/gss_add_oid_set_member.texi
gdoc_TEXINFOS += texi/gss_test_oid_set_member.texi
gdoc_TEXINFOS += texi/gss_release_oid_set.texi
gdoc_TEXINFOS += texi/gss_indicate_mechs.texi
gdoc_TEXINFOS += texi/gss_release_buffer.texi
gdoc_TEXINFOS += texi/gss_get_mic.texi
gdoc_TEXINFOS += texi/gss_verify_mic.texi
gdoc_TEXINFOS += texi/gss_wrap.texi
gdoc_TEXINFOS += texi/gss_unwrap.texi
gdoc_TEXINFOS += texi/gss_import_name.texi
gdoc_TEXINFOS += texi/gss_display_name.texi
gdoc_TEXINFOS += texi/gss_compare_name.texi
gdoc_TEXINFOS += texi/gss_release_name.texi
gdoc_TEXINFOS += texi/gss_inquire_names_for_mech.texi
gdoc_TEXINFOS += texi/gss_inquire_mechs_for_name.texi
gdoc_TEXINFOS += texi/gss_export_name.texi
gdoc_TEXINFOS += texi/gss_canonicalize_name.texi
gdoc_TEXINFOS += texi/gss_duplicate_name.texi
gdoc_TEXINFOS += texi/gss_oid_equal.texi
gdoc_TEXINFOS += texi/gss_inquire_saslname_for_mech.texi
gdoc_TEXINFOS += texi/gss_inquire_mech_for_saslname.texi
gdoc_TEXINFOS += texi/gss_check_version.texi
gdoc_MANS =
gdoc_MANS += man/gss_encapsulate_token.3
gdoc_MANS += man/gss_decapsulate_token.3
gdoc_MANS += man/gss_init_sec_context.3
gdoc_MANS += man/gss_accept_sec_context.3
gdoc_MANS += man/gss_delete_sec_context.3
gdoc_MANS += man/gss_process_context_token.3
gdoc_MANS += man/gss_context_time.3
gdoc_MANS += man/gss_inquire_context.3
gdoc_MANS += man/gss_wrap_size_limit.3
gdoc_MANS += man/gss_export_sec_context.3
gdoc_MANS += man/gss_import_sec_context.3
gdoc_MANS += man/gss_acquire_cred.3
gdoc_MANS += man/gss_add_cred.3
gdoc_MANS += man/gss_inquire_cred.3
gdoc_MANS += man/gss_inquire_cred_by_mech.3
gdoc_MANS += man/gss_release_cred.3
gdoc_MANS += man/gss_display_status.3
gdoc_MANS += man/gss_userok.3
gdoc_MANS += man/gss_create_empty_oid_set.3
gdoc_MANS += man/gss_add_oid_set_member.3
gdoc_MANS += man/gss_test_oid_set_member.3
gdoc_MANS += man/gss_release_oid_set.3
gdoc_MANS += man/gss_indicate_mechs.3
gdoc_MANS += man/gss_release_buffer.3
gdoc_MANS += man/gss_get_mic.3
gdoc_MANS += man/gss_verify_mic.3
gdoc_MANS += man/gss_wrap.3
gdoc_MANS += man/gss_unwrap.3
gdoc_MANS += man/gss_import_name.3
gdoc_MANS += man/gss_display_name.3
gdoc_MANS += man/gss_compare_name.3
gdoc_MANS += man/gss_release_name.3
gdoc_MANS += man/gss_inquire_names_for_mech.3
gdoc_MANS += man/gss_inquire_mechs_for_name.3
gdoc_MANS += man/gss_export_name.3
gdoc_MANS += man/gss_canonicalize_name.3
gdoc_MANS += man/gss_duplicate_name.3
gdoc_MANS += man/gss_oid_equal.3
gdoc_MANS += man/gss_inquire_saslname_for_mech.3
gdoc_MANS += man/gss_inquire_mech_for_saslname.3
gdoc_MANS += man/gss_check_version.3

BUILT_SOURCES = $(gdoc_MANS) $(gdoc_TEXINFOS)

GDOC_SRC = $(top_srcdir)/lib/*.c

.PHONY: compare-makefile

compare-makefile:
	$(AM_V_GEN) \
	MANS=""; \
	TEXINFOS=""; \
	FUNCS=`$(srcdir)/gdoc -listfunc $(GDOC_SRC)`; \
	for i in $$FUNCS; do \
		MANS="$$MANS\ngdoc_MANS += man/$$i.3"; \
		TEXINFOS="$$TEXINFOS\ngdoc_TEXINFOS += texi/$$i.texi"; \
	done && \
	grep -v -e '^gdoc_MANS += ' -e '^gdoc_TEXINFOS += ' $(srcdir)/Makefile.am | \
		perl -p -e "s,^gdoc_MANS =,gdoc_MANS =$$MANS,;" | \
		perl -p -e "s,^gdoc_TEXINFOS =,gdoc_TEXINFOS =$$TEXINFOS,;" \
		> tmp-$@ && \
	diff -u $(srcdir)/Makefile.am tmp-$@ && \
	rm -f tmp-$@

EXTRA_DIST += stamp-gdoc

$(gdoc_MANS) $(gdoc_TEXINFOS): stamp-gdoc

clean-local:
	-rm -f stamp-gdoc

stamp-gdoc: $(top_srcdir)/.version $(GDOC_SRC)
	$(AM_V_at)$(MKDIR_P) man texi
	$(AM_V_GEN)for i in `$(srcdir)/gdoc -listfunc $(GDOC_SRC)`; do \
		$(srcdir)/gdoc -man \
			-bugsto $(PACKAGE_BUGREPORT) \
			-module $(PACKAGE) \
			-pkg-name "$(PACKAGE_NAME)" \
			-sourceversion $(VERSION) \
			-includefuncprefix \
			-seeinfo $(PACKAGE) \
			-verbatimcopying \
			-copyright "2003-2022 Simon Josefsson" \
			-function $$i \
			$(GDOC_SRC) > man/$$i.3 && \
		$(srcdir)/gdoc -texinfo -function $$i \
			$(GDOC_SRC) > texi/$$i.texi; \
	done
	$(AM_V_at)touch $@
